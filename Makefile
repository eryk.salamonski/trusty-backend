build_hard_hat_img:
	cd hard_hat && docker build . -t hhdocker && cd ..

run_hard_hat_img:
	cd hard_hat && docker run -it -d -p 8545:8545 --name hard_hat hhdocker && cd ..

logs_hard_hat:
	cd hard_hat && docker logs hard_hat --follow && cd .. 

remove_hard_hat:
	cd hard_hat && docker rm hard_hat && cd ..

compile_contract:
	cd hard_hat && docker exec -it myhd yarn compile:local && cd ..

deploy_contract:
	cd hard_hat && docker exec -it myhd yarn deploy:local && cd .. 
